using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ActionManager : MonoBehaviour
{
    public GrapplingGun grapple;
    public GameObject paintCan;
    public float paintRange;
    public Transform camera;
    public bool painting = false;
    public LayerMask paintLayer;
    public float transitionTime;
    public bool transitioning = false;
    public Text text;
    private int boardsPainted = 0;
    private const int boardsToPaint = 5;
    public Color[] colors;
    public Material canMat;
    private int curColorIndex = 0;
    public TexturePainter painter = null;
    public AudioSource shakeSound;
    public AudioSource paintSound;
    // Start is called before the first frame update
    void Start()
    {
        canMat.color = colors[curColorIndex];
    }

    // Update is called once per frame
    void Update()
    {
        RaycastHit hit;

        if (Physics.Raycast(camera.position, camera.forward, out hit, paintRange, paintLayer))
        {
            
            if (!painting && !transitioning)
            {
                StartCoroutine(EquipPaint());
                painter = hit.collider.GetComponent<PainterRef>().painter;
                painting = true;
            }
            
        }
        else
        {
            if(painting && !transitioning)
            {
                StartCoroutine(EquipGrapple());
                painter = null;
                painting = false;
            }
        }
        if(Input.GetKeyDown(KeyCode.Q) && painting)
        {
            curColorIndex--;
            curColorIndex = curColorIndex < 0 ? colors.Length-1 : curColorIndex;
            UpdateColor();
        }
        if (Input.GetKeyDown(KeyCode.E) && painting)
        {
            curColorIndex++;
            curColorIndex = curColorIndex >= colors.Length ? 0 : curColorIndex;
            UpdateColor();
        }
        if (Input.GetKeyDown(KeyCode.C))
        {
            if(painter != null)
            {
                painter.ClearBrushes();
            }
        }
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
    }

    void UpdateColor()
    {
        shakeSound.Play();
        canMat.color = colors[curColorIndex];
    }

    public Color GetColor()
    {
        return colors[curColorIndex];
    }

    public void BoardPainted()
    {
        boardsPainted++;
        if(boardsPainted >= boardsToPaint)
        {
            text.text = "Thanks for Playing!";
        }
        else
        {
            text.text = "Boards Painted: " + boardsPainted + "/" + boardsToPaint;
        }
        
    }

    IEnumerator EquipGrapple()
    {
        transitioning = true;
        GameObject obj = paintCan;
        float curTime = 0;
        float y = -.5f;
        while(curTime <= transitionTime)
        {
            curTime += Time.deltaTime;
            obj.transform.localPosition = new Vector3(obj.transform.localPosition.x, Mathf.Lerp(y, -1, curTime / transitionTime), obj.transform.localPosition.z);
            obj.transform.localRotation = Quaternion.Lerp(Quaternion.identity, Quaternion.Euler(45, 0, 0), curTime / transitionTime);
            yield return new WaitForEndOfFrame();

        }
        obj.SetActive(false);
        
        obj = grapple.gameObject;
        obj.SetActive(true);
        grapple.swingingSound.Stop();
        curTime = 0;
        y = -1;
        while (curTime <= transitionTime)
        {
            curTime += Time.deltaTime;
            obj.transform.localPosition = new Vector3(obj.transform.localPosition.x, Mathf.Lerp(y, -.5f, curTime / transitionTime), obj.transform.localPosition.z);
            obj.transform.localRotation = Quaternion.Lerp(Quaternion.Euler(45, 0, 0), Quaternion.identity, curTime / transitionTime);
            yield return new WaitForEndOfFrame();

        }
        grapple.canGrapple = true;
        transitioning = false;
    }

    IEnumerator EquipPaint()
    {
        transitioning = true;
        grapple.StopGrapple();
        grapple.canGrapple = false;
        GameObject obj = grapple.gameObject;
        float curTime = 0;
        float y = -.5f;
        while (curTime <= transitionTime)
        {
            curTime += Time.deltaTime;
            obj.transform.localPosition = new Vector3(obj.transform.localPosition.x, Mathf.Lerp(y, -1, curTime / transitionTime), obj.transform.localPosition.z);
            obj.transform.localRotation = Quaternion.Lerp(Quaternion.identity, Quaternion.Euler(45, 0, 0), curTime / transitionTime);
            yield return new WaitForEndOfFrame();

        }
        obj.SetActive(false);
        
        obj = paintCan;
        obj.SetActive(true);
        curTime = 0;
        y = -1;
        while (curTime <= transitionTime)
        {
            curTime += Time.deltaTime;
            obj.transform.localPosition = new Vector3(obj.transform.localPosition.x, Mathf.Lerp(y, -.5f, curTime / transitionTime), obj.transform.localPosition.z);
            obj.transform.localRotation = Quaternion.Lerp(Quaternion.Euler(45, 0, 0), Quaternion.identity, curTime / transitionTime);
            yield return new WaitForEndOfFrame();

        }
        
        transitioning = false;
    }
}
