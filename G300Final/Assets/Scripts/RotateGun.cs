using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateGun : MonoBehaviour
{
    public float rotSpeed = 5f;
    public GrapplingGun grappling;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(grappling.canGrapple)
        {
            Quaternion rot = transform.parent.rotation;
            if (grappling.IsGrappling())
                rot = Quaternion.LookRotation(grappling.GetGrapplePoint() - transform.position);

            //transform.rotation = rot;
            transform.rotation = Quaternion.Lerp(transform.rotation, rot, rotSpeed * Time.deltaTime);
        }
        
    }
}
