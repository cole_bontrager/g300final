using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerReset : MonoBehaviour
{
    private Vector3 spawnPos;
    public float yReset;
    // Start is called before the first frame update
    void Start()
    {
        spawnPos = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.position.y < yReset)
        {
            transform.position = spawnPos;
            GetComponent<Rigidbody>().velocity = Vector3.zero;
        }
    }

    public void SetReset(Vector3 pos)
    {
        spawnPos = pos;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Reset")
        {
            GetComponent<PlayerMovement>().grapple.StopGrapple();
            spawnPos = other.transform.position;
        }
    }
}
