using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrapplingGun : MonoBehaviour
{
    private LineRenderer lineRenderer;
    private Vector3 grapplePoint;
    public LayerMask grappleLayer;
    public Transform gunTip, camera, player;
    public float grappleDistance = 100f;
    private SpringJoint joint;
    public GameObject visual;
    private bool grappling = false;
    public bool canGrapple = true;
    public PlayerMovement movement;
    public AudioSource grappleSound;
    public AudioSource swingingSound;
    // Start is called before the first frame update
    private void Awake()
    {
        lineRenderer = GetComponent<LineRenderer>();
    }

    private void Update()
    {
        RaycastHit hit;

        if (Physics.Raycast(camera.position, camera.forward, out hit, grappleDistance, grappleLayer) || grappling)
        {
            visual.SetActive(true);
        }
        else
        {
            visual.SetActive(false);
        }
        if (Input.GetMouseButtonDown(0) && canGrapple)
        {
            StartGrapple();
        }
        else if (Input.GetMouseButtonUp(0))
        {
            StopGrapple();
        }
    }

    private void LateUpdate()
    {
        DrawRope();
    }

    void StartGrapple()
    {
        
        RaycastHit hit;

        if(Physics.Raycast(camera.position, camera.forward, out hit, grappleDistance, grappleLayer))
        {
            grappleSound.Play();
            swingingSound.Play();
            movement.dashReady = true;
            grappling = true;
            grapplePoint = hit.point;
            joint = player.gameObject.AddComponent<SpringJoint>();
            joint.autoConfigureConnectedAnchor = false;
            joint.connectedAnchor = hit.point;

            float distanceFromPoint = Vector3.Distance(player.position, grapplePoint);

            joint.maxDistance = distanceFromPoint * .8f;
            joint.minDistance = distanceFromPoint * .25f;
            
            joint.spring = 4.5f;
            joint.damper = 7f;
            joint.massScale = 4.5f;

            if(hit.rigidbody != null)
            {
                joint.connectedAnchor = hit.collider.gameObject.transform.InverseTransformPoint(grapplePoint);
                joint.connectedBody = hit.rigidbody;
            }
            lineRenderer.positionCount = 2;
        }
    }

    void DrawRope()
    {
        if (!joint) return;
        lineRenderer.SetPosition(0, gunTip.position);
        if(joint.connectedBody != null)
        {
            lineRenderer.SetPosition(1, joint.connectedBody.position + joint.connectedAnchor);
        }
        else
        {
            lineRenderer.SetPosition(1, grapplePoint);
        }
        
    }

    public void StopGrapple()
    {
        swingingSound.Stop();
        grappling = false;
        lineRenderer.positionCount = 0;
        Destroy(joint);
    }

    public bool IsGrappling()
    {
        return joint != null;
    }

    public Vector3 GetGrapplePoint()
    {
        if(joint == null)
        {
            return Vector3.zero;
        }
        else if(joint.connectedBody != null)
        {
            return joint.connectedBody.position + joint.connectedAnchor;
        }
        else
        {
            return grapplePoint;
        }
        
    }
}
